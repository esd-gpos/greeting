package main

import (
	"encoding/json"
	"log"
	"net/http"
)

func main() {

	// Register handle func to default serve mux
	http.HandleFunc("/", handlerGenerator("I am root, go away."))
	http.HandleFunc("/healthz", handlerGenerator("I am in good health."))

	// nil indicates that we will use default serve mux
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", nil))
}

func handlerGenerator(msg string) http.HandlerFunc {
	response := struct{ Message string `json:"message"` }{Message: msg}
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		err := json.NewEncoder(w).Encode(response)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}
